import grpc
from concurrent import futures
import time
# import the generated classes
import healt_pb2
import healt_pb2_grpc
import sys, getopt
import os
from flask import Flask
from flask import request
import json , jsonify


app = Flask(__name__, instance_relative_config=True)

class Database:
    nodes = []

    def Update(self,node, values):
        exist = False
        for x in self.nodes:
            if( x["NAME"] == node ):
                x["ITEMS"] = values
                exist = True
        
        if not ( exist  ):
            self.nodes.append({'NAME':node,'ITEMS':values})
            
    def Get(self):
        return json.dumps(self.nodes)



Db = Database()              


# default routing for invalid Routes
@app.route('/', defaults={'path': ''},methods = ['POST'])
@app.route('/<path:path>',methods = ['POST'])
def catch_all(path):

    #rerouting    
    print ('Obtaining Data')
    return Db.Get(), 200, {'ContentType':'application/json'} 
    


# create a class to define the server functions, derived from
# calculator_pb2_grpc.HealthServicer
class HealthServicer(healt_pb2_grpc.HealthServicer):

    # calculator.square_root is exposed here
    # the request and response are of the data type
    # calculator_pb2.Number
    def Check(self, request, context):
        print("LOG:" + str(request))
        response = healt_pb2.HealthCheckResponse()
        index = 0 
        values = [] 
        for i in request.items:
             values.append({'key':i.key,'value':i.value})       
        
        Db.Update(request.node_name,values)

        return response





def main(argv):

   ResourceControlServer_host = 'localhost' 
   ResourceControlServer_port = 5000
   debug_flask = False

      # create a gRPC server
   server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

   # use the generated function `add_HealthServicer_to_server`
   # to add the defined class to the server
   healt_pb2_grpc.add_HealthServicer_to_server(
        HealthServicer(), server)

   # listen on port 50051
   print('Starting server. Listening on port 50051.')
   server.add_insecure_port('[::]:50051')
   server.start()
   #print info 
   print ('Resource Control Server ... ')

   #Start http server
   app.run(threaded=True,host = ResourceControlServer_host ,port=ResourceControlServer_port, debug = debug_flask )
   
   server.stop(0)

if __name__ == "__main__":
   main(sys.argv[1:])